//  How to access GPIO registers from C-code on the Raspberry-Pi
//  Example program
//  15-January-2012
//  Dom and Gert
//


// Access from ARM Running Linux

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <bcm2835.h>
#include <unistd.h>

#define MAXTIMINGS 100

#define DHT11 11

int readDHT(int type, int pin);

int main(int argc, char **argv)
{

//	const time_t timer = time(NULL);
//	printf("%s", ctime(&timer));

 if (!bcm2835_init())
       return 1;

	int type = DHT11;

 int dhtpin = 4;


 printf("Using pin #%d\n", dhtpin);

readDHT(type, dhtpin);
 return 0;

} // main


int bits[250], data[100];
int bitidx = 0;

int readDHT(int type, int pin) {
 int counter = 0;
 int laststate = HIGH;
 int j=0;
 int i=0;
 // Set GPIO pin to output
 bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
 bcm2835_gpio_write(pin, HIGH);
 usleep(100);
 bcm2835_gpio_write(pin, LOW);
 usleep(20000);
 bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);

 data[0] = data[1] = data[2] = data[3] = data[4] = 0;
 // read data!

 for (i=0; i< MAXTIMINGS; i++) {
    counter = 0;
    while ( bcm2835_gpio_lev(pin) == laststate) {
       counter++;
       nanosleep(1);           // overclocking might change this?
       if (counter == 100)
         break;
    }
    laststate = bcm2835_gpio_lev(pin);
    if (counter == 100) break;
    bits[bitidx++] = counter;

    if ((i>3) && (i%2 == 0)) {
     // shove each bit into the storage bytes
     data[j/8] <<= 1;
     if (counter > 16)
       data[j/8] |= 1;
     j++;
    }
 }


#ifdef DEBUG
 for (int i=3; i<bitidx; i+=2) {
    printf("bit %d: %d\n", i-3, bits[i]);
    printf("bit %d: %d (%d)\n", i-2, bits[i+1], bits[i+1] > 15);
 }
#endif

 printf("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x\n", j, data[0], data[1], data[2], data[3], data[4]);

 if ((j >= 39) &&
     (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {
    // yay!

	printf("Temp = %d *C, Hum = %d \%\n", data[2], data[0]);
	
	FILE *f = fopen("/var/www/data/temp.log", "w");
	if (f!=NULL) {
		fprintf(f, "{\"temp\":\"%d\",\"hum\":\"%d\"}", data[2], data[0]);
		fclose(f);
	}
    
    return 1;
 }

 return 0;
}

